<?php

$EM_CONF['browser_update'] = [
    'title' => 'Browser Update',
    'category' => 'misc',
    'state' => 'alpha',
    'version' => '0.0.1',
    'author' => 'Thomas Anders',
    'author_email' => 'me@naderio.de',
    'author_company' => 'naderio.de',
    'clearCacheOnLoad' => 1,
    'constraints' => [
        'depends' => [
            'typo3' => '11.5.0-11.5.99',
        ],
    ],
];
