<?php
declare(strict_types=1);

defined('TYPO3') || die();

\TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addStaticFile(
    'browser_update',
    'Configuration/TypoScript',
    'Browser Update (main)'
);
