<?php

declare(strict_types=1);

namespace Naderio\BrowserUpdate\UserFunc;

use TYPO3\CMS\Core\Utility\MathUtility;

class BrowserUpdate
{
    public const BROWSERS = [
        'edge' => 'e',
        'firefox' => 'f',
        'opera' => 'o',
        'safari' => 's',
        'chrome' => 'c',
    ];

    public const DEFAULT_BROWSER_VERSIONS = [
        'e' => -4,
        'f' => -3,
        'o' => -3,
        's' => -1,
        'c' => -3,
    ];

    public const CONFIG_KEYS = [
        'insecure',
        'unsupported',
        'api',
    ];

    public const DEFAULT_CONFIG = [
        'insecure' => 'true',
        'unsupported' => 'true',
        'api' => '2022.02',
    ];

    public function scriptTag($content, $conf)
    {
        $browsers = $this->getBrowserVersions($conf);
        $configuration = $this->getConfiguration($conf);

        return sprintf(
            '<script>
var $buoop = {
    required:{e:%1$s,f:%2$s,o:%3$s,s:%4$s,c:%5$s},
    insecure:%6$s, unsupported:%7$s, api:"%8$s"
};
function $buo_f(){ var e = document.createElement("script"); e.src = "//browser-update.org/update.min.js"; document.body.appendChild(e); } try {document.addEventListener("DOMContentLoaded", $buo_f,false)} catch(e){window.attachEvent("onload", $buo_f)}
</script>',
            $browsers['e'],
            $browsers['f'],
            $browsers['o'],
            $browsers['s'],
            $browsers['c'],
            $configuration['insecure'],
            $configuration['unsupported'],
            $configuration['api'],
        );
    }

    private function getBrowserVersions(array $config): array
    {
        $browsers = [];
        foreach (self::BROWSERS as $browser => $short) {
            if (MathUtility::canBeInterpretedAsInteger($config['settings.'][$browser])) {
                $browsers[$short] = (int)$config['settings.'][$browser];
            } else {
                $browsers[$short] = self::DEFAULT_BROWSER_VERSIONS[$short];
            }
        }
        return $browsers;
    }

    private function getConfiguration(array $config): array
    {
        $configuration = [];
        foreach (self::CONFIG_KEYS as $key) {

            if (! empty($config['settings.'][$key])) {
                $configuration[$key] = (string)$config['settings.'][$key];
            } else {
                $configuration[$key] = self::DEFAULT_CONFIG[$key];
            }
        }
        return $configuration;
    }
}
